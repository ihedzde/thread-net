using Newtonsoft.Json;
using System;

namespace Thread_.NET.Common.DTO.Auth
{
    public class ResetDTO
    {
       public string Email { get; set; }
       public string NewPassword { get; set; }

    }
}
