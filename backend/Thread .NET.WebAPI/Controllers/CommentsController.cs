﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Extensions;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly CommentService _commentService;
        private readonly LikeService _likeService;

        public CommentsController(CommentService commentService, LikeService likeService)
        {
            _commentService = commentService;
            _likeService = likeService;
        }
        [HttpPut]
        public async Task<ActionResult<CommentDTO>> UpdatePost([FromBody] UpdateCommentDTO dto)
        {
            dto.AuthorId = this.GetUserIdFromToken();
    
            return Ok(await _commentService.UpdateComment(dto));
        }
        [HttpPost]
        public async Task<ActionResult<CommentDTO>> CreatePost([FromBody] NewCommentDTO comment)
        {
            comment.AuthorId = this.GetUserIdFromToken();
            return Ok(await _commentService.CreateComment(comment));
        }
        [HttpPost("like")]
        public async Task<IActionResult> LikeComment(NewReactionDTO reaction)
        {
            reaction.UserId = this.GetUserIdFromToken();

            await _likeService.LikeComment(reaction);
            return Ok();
        }
        [HttpPost("dislike")]
        public async Task<IActionResult> DislikeComment(NewReactionDTO reaction)
        {
            reaction.UserId = this.GetUserIdFromToken();

            await _likeService.LikeComment(reaction);
            return Ok();
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePost(int id){
            var userId = this.GetUserIdFromToken();
            try{
                return Ok(await _commentService.DeleteComment(id, userId));
            }catch(Exception e){
                return BadRequest(e.Message);
            }
        }
    }
}