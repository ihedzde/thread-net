﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.Auth;
using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [AllowAnonymous]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly AuthService _authService;

        public AuthController(AuthService authService)
        {
            _authService = authService;
        }

        [HttpPost("login")]
        public async Task<ActionResult<AuthUserDTO>> Login(UserLoginDTO dto)
        {
            return Ok(await _authService.Authorize(dto));
        }
         [HttpPut("reset")]
        public async Task<IActionResult> ResetPass(ResetDTO dto)
        {
            try{
                await _authService.ResetPass(dto, "","");//TODO add your custome SMTP email and password.
                return Ok("Check your email for reset.");
            }catch(Exception e){
                return BadRequest(e.Message);
            }
            
        }
        [HttpGet("configReset")]
        public async Task<IActionResult> ConfigReset(string token)
        {
            try{

                await _authService.ConfigPassSave(token);
                return new ContentResult {
                    ContentType = "text/html",
                    StatusCode = (int) HttpStatusCode.OK,
                    Content = "<html><meta http-equiv=\"refresh\" content=\"3;url=https://localhost:4200\" /><body>Password updated.</body></html>"
                };
            }catch(Exception e){
                return BadRequest(e.Message);
            }
            
        }
    }
}