﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;
using Thread_.NET.BLL.Exceptions;
namespace Thread_.NET.BLL.Services
{
    public sealed class PostService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;

        public PostService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper)
        {
            _postHub = postHub;
        }

        public async Task<ICollection<PostDTO>> GetAllPosts()
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Reactions)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .OrderByDescending(post => post.CreatedAt)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<ICollection<PostDTO>> GetAllPosts(int userId)
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .Where(p => p.AuthorId == userId) // Filter here
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }
        public async Task<PostDTO> UpdatePost(PostUpdateDTO postDto)
        {
            var oldPost = _context.Posts.FirstOrDefault(x => x.Id == postDto.PostId);
            if (oldPost == null)
            {
                throw new NotFoundException(nameof(Post), postDto.PostId);
            }

            var postEntity = _mapper.Map<PostUpdateDTO, Post>(postDto, oldPost);
            if (string.IsNullOrEmpty(postDto.PreviewImage)){
                postEntity.PreviewId = null;
            }
            _context.Posts.Update(postEntity);
            await _context.SaveChangesAsync();

            var updatedPost = await _context.Posts
                .Include(post => post.Author)
					.ThenInclude(author => author.Avatar)
                .FirstAsync(post => post.Id == postEntity.Id);

            var updatedPostDTO = _mapper.Map<PostDTO>(updatedPost);
            await _postHub.Clients.All.SendAsync("UpdatePost", updatedPostDTO);

            return updatedPostDTO;
        }
        
        public async Task<PostDTO> CreatePost(PostCreateDTO postDto)
        {
            var postEntity = _mapper.Map<Post>(postDto);

            _context.Posts.Add(postEntity);
            await _context.SaveChangesAsync();

            var createdPost = await _context.Posts
                .Include(post => post.Author)
					.ThenInclude(author => author.Avatar)
                .FirstAsync(post => post.Id == postEntity.Id);

            var createdPostDTO = _mapper.Map<PostDTO>(createdPost);
            await _postHub.Clients.All.SendAsync("NewPost", createdPostDTO);

            return createdPostDTO;
        }
        public async Task<PostDTO> DeletePost(int postId, int userId)
        {
            var post =  _context.Posts
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                .Include(post => post.Comments).ThenInclude(comment=> comment.Reactions)
                .FirstOrDefault(p => p.Id == postId);
            if (post == null || post.AuthorId != userId)
                throw new ArgumentException();
            foreach (var comment in post.Comments){
                _context.CommentReactions.RemoveRange(comment.Reactions);
            }
            await _context.SaveChangesAsync();
            _context.Comments.RemoveRange(post.Comments);
            await _context.SaveChangesAsync();
            _context.Posts.Remove(post);
            await _context.SaveChangesAsync();
            var postDTO = _mapper.Map<PostDTO>(post);
            await _postHub.Clients.All.SendAsync("DeletedPost", post);

            return postDTO;
        }
    }
}
