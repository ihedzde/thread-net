﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        public CommentService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            return _mapper.Map<CommentDTO>(createdComment);
        }
        public async Task<CommentDTO> UpdateComment(UpdateCommentDTO commentDTO)
        {
            var oldComment = _context.Comments.FirstOrDefault(x => x.Id == commentDTO.CommentId);
            if (oldComment == null)
            {
                throw new NotFoundException(nameof(Comment), commentDTO.CommentId);
            }

            var commentEntity = _mapper.Map<UpdateCommentDTO, Comment>(commentDTO, oldComment);
            _context.Comments.Update(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            return _mapper.Map<CommentDTO>(createdComment);
        }
         public async Task<CommentDTO> DeleteComment(int commentId, int userId)
        {
            var comment =  _context.Comments
                .Include(c => c.Reactions)
                .FirstOrDefault(c => c.Id == commentId);
            if (comment == null || comment.AuthorId != userId)
                throw new ArgumentException();

            _context.Comments.Remove(comment);
            await _context.SaveChangesAsync();
            var commentDTO = _mapper.Map<CommentDTO>(comment);

            return commentDTO;
        }
    }
}
