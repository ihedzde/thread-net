﻿using System;
using Thread_.NET.DAL.Entities.Abstract;

namespace Thread_.NET.DAL.Entities
{
    public sealed class ResetToken : BaseEntity
    {
        private const int MINUTES_TO_EXPIRE = 5;

        public ResetToken()
        {
            Expires = DateTime.UtcNow.AddMinutes(MINUTES_TO_EXPIRE);
        }

        public string Token { get; set; }
        public string PasswordHash {get; set;}
        public string Salt {get; set;}
        public DateTime Expires { get; private set; }

        public int UserId { get; set; }
        public User User { get; set; }

        public bool IsActive => DateTime.UtcNow <= Expires;
    }
}
