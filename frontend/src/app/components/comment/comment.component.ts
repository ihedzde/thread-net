import { AuthenticationService } from './../../services/auth.service';
import { AuthDialogService } from './../../services/auth-dialog.service';
import { LikeService } from './../../services/like.service';
import { Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { User } from 'src/app/models/user';
import { empty, Observable, Subject } from 'rxjs';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { DialogType } from 'src/app/models/common/auth-dialog-type';
import { CommentService } from '../../services/comment.service';
import { Reaction } from 'src/app/models/reactions/reaction';
@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent implements OnDestroy {
    @Input() public comment: Comment;
    @Input() public currentUser: User;
    @Output() delete: EventEmitter<string> = new EventEmitter();
    public editComment = false;
    private unsubscribe$ = new Subject<void>();
    public constructor(
        private likeService: LikeService,
        private commentService: CommentService,
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService
    ) {
        this.initCurrentUser();
     }
    private initCurrentUser(){
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) 
                        this.currentUser = user;
                });
        }
    }
    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
    public getLikeCount(): number {
        return this.comment.reactions.filter((x) => x.isLike === true).length;
    }
    public getLiked(): Reaction[] {
        return this.comment.reactions.filter((x) => x.isLike);
    }
    public getDislikeCount() {
        return this.comment.reactions.filter((x) => x.isLike == false).length;
    }
    public updateComment() {
        if (!this.editComment)
        return;
        this.editComment = false;
        this.commentService.updateComment({
            commentId: this.comment.id,
            authorId: this.comment.author.id,
            body: this.comment.body
        });
    }
    public deleteComment(){
        this.commentService.deleteComment(this.comment.id);
        this.editComment = false;
        this.delete.emit(this.comment.id.toString());
    }
    public likeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));

            return;
        }
        this.likeService
            .likeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }
    public dislikeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.dislikeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));

            return;
        }
        this.likeService
            .dislikeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }
    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }
    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

}
