import { Reaction } from './../../models/reactions/reaction';
import { UpdatePost } from './../../models/post/update-post';
import { PostService } from './../../services/post.service';
import { Component, Input, OnDestroy } from '@angular/core';
import { Post } from '../../models/post/post';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikeService } from '../../services/like.service';
import { NewComment } from '../../models/comment/new-comment';
import { CommentService } from '../../services/comment.service';
import { User } from '../../models/user';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { Router, ActivatedRoute} from '@angular/router';
@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnDestroy {
    @Input() public post: Post;
    @Input() public currentUser: User;

    public showComments = false;
    public newComment = {} as NewComment;
    public editPost = false;
    public imageFile: File;
    private unsubscribe$ = new Subject<void>();
    
    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private postService: PostService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private route: ActivatedRoute,
        private router: Router
    ) { }
    public ngOnInit() {
        this.route.queryParams.subscribe(params => {
          let postIdFromShare = params['postId'];
          if(postIdFromShare != null){
            this.postService
            .getPosts()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    const post = resp.body.find((x)=>x.id == postIdFromShare);
                    if(post == null){
                        this.router.navigate(['/']);
                    }
                    this.post = post;
                },
                (error) => (this.snackBarService.showErrorMessage(error))
            )
             
          }else{
            this.router.navigate(['/']);
          }
        });
      }
    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }

        this.showComments = !this.showComments;
    }
    public deleteComment(commentId: string){
        const id = parseInt(commentId);
        this.post.comments = this.post.comments.filter((c)=> c.id !==id);
        
    }
    public deletePost(){
        this.postService.deletePost(this.post.id);
        this.editPost = false;
    }
    public updatePost(){
        if (!this.editPost)
            return;
        this.editPost = false;
        this.postService.updatePost({
            postId: this.post.id,
            authorId: this.post.author.id, 
            body: this.post.body, 
            previewImage: this.post.previewImage
        });
    }
    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.post.previewImage = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }
    public removeImage() {
        this.post.previewImage = '';
        this.imageFile = null;
    }
    public getLikeCount(): number {
        return this.post.reactions.filter((x) => x.isLike).length;
    }
    public getLiked(): Reaction[] {
        return this.post.reactions.filter((x) => x.isLike);
    }
    public async share(){
        if (!navigator.share)  {
                this.snackBarService.showErrorMessage("Error sharing. Browser not supported.")
            }
        const shareData = {
            title: 'Thread Post Share',
            text: 'Checkout your friends new post on Thread.',
            url: `https://localhost:4200/post?postId=${this.post.id}`,
          }
          try {
        await navigator.share(shareData)}
        catch(e){
            console.log(e)
        }
    }
    public getDislikeCount() {
        return this.post.reactions.filter((x) => !x.isLike).length;
    }
    public likePost() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likePost(this.post, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => {this.post = post});

            return;
        }

        this.likeService
            .likePost(this.post, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.post = post));
    }
    public dislikePost() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.dislikePost(this.post, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));

            return;
        }

        this.likeService
            .dislikePost(this.post, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.post = post));
    }
    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(this.post.comments.concat(resp.body));
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }
}
