export interface ResetDto {
    email: string;
    newPassword: string;
}
