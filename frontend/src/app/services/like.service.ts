import { CommentService } from './comment.service';
import { Injectable } from '@angular/core';
import { AuthenticationService } from './auth.service';
import { Post } from '../models/post/post';
import { Comment } from '../models/comment/comment'
import { NewReaction } from '../models/reactions/newReaction';
import { PostService } from './post.service';
import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class LikeService {
    public constructor(private authService: AuthenticationService, private postService: PostService, private commentService: CommentService) { }

    public likeComment(comment: Comment, currentUser: User) {
        const innertComment = comment;
        const reaction: NewReaction = {
            entityId: innertComment.id,
            isLike: true,
            userId: currentUser.id
        }
        let hasReaction = innertComment.reactions.some((x) => x.user.id === currentUser.id && x.isLike == true);
        innertComment.reactions = hasReaction
            ? innertComment.reactions.filter((x) => x.user.id !== currentUser.id || x.isLike !== true)
            : innertComment.reactions.concat({ isLike: true, user: currentUser });
        hasReaction = innertComment.reactions.some((x) => x.user.id === currentUser.id);

        return this.commentService.likeComment(reaction).pipe(
            map(() => innertComment),
            catchError(() => {
                // revert current array changes in case of any error
                innertComment.reactions = hasReaction
                    ? innertComment.reactions.filter((x) => x.user.id !== currentUser.id)
                    : innertComment.reactions.concat({ isLike: true, user: currentUser });

                return of(innertComment);
            })
        );
    }
    public likePost(post: Post, currentUser: User) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: true,
            userId: currentUser.id
        };

        // update current array instantly
        let hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id && x.isLike === true);
        innerPost.reactions = hasReaction
            ? innerPost.reactions.filter((x) => x.user.id !== currentUser.id || x.isLike !== true)
            : innerPost.reactions.concat({ isLike: true, user: currentUser });

        return this.postService.likePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                // revert current array changes in case of any error
                innerPost.reactions = hasReaction
                    ? innerPost.reactions.filter((x) => x.user.id !== currentUser.id)
                    : innerPost.reactions.concat({ isLike: true, user: currentUser });

                return of(innerPost);
            })
        );
    }
    public dislikeComment(comment: Comment, currentUser: User) {
        const innertComment = comment;
        const reaction: NewReaction = {
            entityId: innertComment.id,
            isLike: false,
            userId: currentUser.id
        }
        let hasReaction = innertComment.reactions.some((x) => x.user.id === currentUser.id && x.isLike === false);
        innertComment.reactions = hasReaction
            ? innertComment.reactions.filter((x) => x.user.id !== currentUser.id || x.isLike !== false)
            : innertComment.reactions.concat({ isLike: false, user: currentUser });

        return this.commentService.dislikeComment(reaction).pipe(
            map(() => innertComment),
            catchError(() => {
                // revert current array changes in case of any error
                innertComment.reactions = hasReaction
                    ? innertComment.reactions.filter((x) => x.user.id !== currentUser.id)
                    : innertComment.reactions.concat({ isLike: false, user: currentUser });

                return of(innertComment);
            })
        );
    }
    public dislikePost(post: Post, currentUser: User) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: false,
            userId: currentUser.id
        };

        // update current array instantly
        let hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id && x.isLike === false);
        innerPost.reactions = hasReaction
            ? innerPost.reactions.filter((x) => x.user.id !== currentUser.id || x.isLike !== false)
            : innerPost.reactions.concat({ isLike: false, user: currentUser });

        return this.postService.dislikePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                // revert current array changes in case of any error
                innerPost.reactions = hasReaction
                    ? innerPost.reactions.filter((x) => x.user.id !== currentUser.id)
                    : innerPost.reactions.concat({ isLike: false, user: currentUser });

                return of(innerPost);
            })
        );
    }
}
